/* global QUnit */

sap.ui.require(["zsc004/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
