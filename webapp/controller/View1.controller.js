sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/m/MessageBox",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageBox, Filter, FilterOperator) {
        "use strict";

        return Controller.extend("zsc004.controller.View1", {
            onInit: function () {

            },
            onDelete(oEvent) {
                var oContext = oEvent.getSource().getBindingContext();
                oContext.delete(oContext.getModel().getGroupId()).then(function () {
                    MessageBox.alert(oContext.getPath(), {
                        icon: MessageBox.Icon.SUCCESS,
                        title: "Success"
                    });
                });
            },
            onAdd() {
                this.byId("idCreateRowDialog").open();
            },
            onConfirmDialog() {
                var oContext = this.byId("idDataTable").getBinding("items").create({
                    M_ID: this.getView().byId("idInputM_id").getValue(),
                    L_ID: this.getView().byId("idInputL_id").getValue(),
                    DESCRIPTION_ZF: this.getView().byId("idInputDescription_zf").getValue(),
                    DESCRIPTION_EN: this.getView().byId("idInputDescription_en").getValue(),
                    ACTIVE: this.getView().byId("idInputActive").getValue()
                });

                this.byId("idCreateRowDialog").close();
                MessageBox.success("Add Row Successful");
            },
            onRejectDialog() {
                this.byId("idCreateRowDialog").close();
            },
            onFormula() {
                var xnavservice = sap.ushell && sap.ushell.Container && sap.ushell.Container.getService("CrossApplicationNavigation");
                var href = (xnavservice && xnavservice.hrefForExternal({
                    target: { semanticObject: "zsc005", action: "display" },
                    params: { "parameter": this.getView().byId("idInputM_id").getValue() }
                })) || "";

                var finalUrl = window.location.href.split("#")[0] + href;
                sap.m.URLHelper.redirect(finalUrl, true);
            },
            onSearch() {
                var sMid = this.getView().byId("idSearchFieldMID").getValue();
                var aFilters = [];
                if (sMid) {
                    var afilter = new Filter("M_ID", FilterOperator.EQ, sMid);
                    aFilters.push(afilter);
                }

                var oTable = this.byId("idDataTable");
                var oBinding = oTable.getBinding("items");
                oBinding.filter(aFilters);

            }
        });
    });
